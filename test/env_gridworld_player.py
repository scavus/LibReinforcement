#!/usr/bin/env python
import sys
import pygame
import numpy as np
import rl_algorithm as rl
from env_gridworld import *

start_pos = Point2d(4, 0)
goal_pos = Point2d(110, 110)
dim = 120
env = Gridworld(dim, start_pos, goal_pos, portal_enable=True, wind_enable=True)
env.portal = (Point2d(2, 0), Point2d(50, 50))
env.wind_row = 45
env.wind_dir = WIND_DIR_L
env.wind_prob = 0.8

algorithm = sys.argv[1]

np.random.seed(0)
q_table = None

if algorithm == 'mc':
	settings = rl.SettingsMc()
	settings.max_ep_count = 500
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, _ = rl.train_mc_first_visit(env, settings)
elif algorithm == 'ql':
	settings = rl.SettingsQLearning()
	settings.max_ep_count = 500
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	settings.gamma = 0.95
	q_table, _ = rl.train_q_learning(env, settings)
elif algorithm == 'sarsa':
	settings = rl.SettingsSarsa()
	settings.max_ep_count = 500
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, _ = rl.train_sarsa(env, settings)
elif algorithm == 'ql-et':
	settings = rl.SettingsQlearningEt()
	settings.max_ep_count = 500
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, _ = rl.train_q_learning_et(env, settings)
elif algorithm == 'dq':
	settings = rl.SettingsDynaQ()
	settings.max_ep_count = 500
	settings.N = 4
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, _ = rl.train_dyna_q(env, settings)
elif algorithm == 'ps':
	settings = rl.SettingsPrioritizedSweeping()
	settings.max_ep_count = 5000
	settings.N = 4
	# settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, _ = rl.train_prioritized_sweeping(env, settings)
elif algorithm == 'ql-fa':
	settings = rl.SettingsQLearningFa()
	settings.max_ep_count = 1000
	settings.approximator_fn = ApproximatorRbfFn()
	q_table, _ = rl.train_q_learning_fa(env, settings)
elif algorithm == 'sarsa-fa':
	settings = rl.SettingsSarsaFa()
	settings.max_ep_count = 1000
	settings.approximator_fn = ApproximatorRbfFn()
	q_table, _ = rl.train_sarsa_fa(env, settings)
elif algorithm == 'dq-fa':
	settings = rl.SettingsDynaQFa()
	settings.max_ep_count = 1000
	settings.alpha = 0.01
	settings.N = 50
	settings.approximator_fn = ApproximatorRbfFn()
	q_table, _ = rl.train_dyna_q_fa(env, settings)
else:
	print('ERROR')

env.reset()

'''
filename = sys.argv[1]
q_table = np.load(filename)
'''

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

WIDTH = 6
HEIGHT = 6

# This sets the margin between each cell
MARGIN = 2

grid = []
for row in range(dim):
	grid.append([])
	for column in range(dim):
		grid[row].append(0)  # Append a cell

grid[start_pos.y][start_pos.x] = 1
grid[goal_pos.y][goal_pos.x] = 2

pygame.init()

WINDOW_SIZE = [(WIDTH+MARGIN)*dim + MARGIN, (HEIGHT+MARGIN)*dim + MARGIN]
screen = pygame.display.set_mode(WINDOW_SIZE)

pygame.display.set_caption("GridworldPlayer")

done = False

clock = pygame.time.Clock()

while not done:
	# policy_fn = rl.PolicyGreedyFn()
	policy_fn = rl.PolicyEGreedyFn(epsilon=0.01)
	grid[env.agent_pos.y][env.agent_pos.x] = 0

	a = None
	if '-fa' in algorithm:
		a = policy_fn(q_table(env, env.get_agent_state()))
	else:
		a = policy_fn(q_table[env.get_agent_state()])

	_, _, done = env.step(a)
	grid[env.agent_pos.y][env.agent_pos.x] = 1

	# Set the screen background
	screen.fill(BLACK)

	# Draw the grid
	for row in range(dim):
		for column in range(dim):
			color = WHITE
			if grid[row][column] == 1:
				color = GREEN
			if grid[row][column] == 2:
				color = RED
			pygame.draw.rect(screen, color,
							 [(MARGIN + WIDTH) * column + MARGIN,
							  (MARGIN + HEIGHT) * row + MARGIN, WIDTH, HEIGHT])

	clock.tick(30)

	pygame.display.flip()

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True

pygame.quit()
