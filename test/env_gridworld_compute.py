#!/usr/bin/env python
import sys
import numpy as np
import rl_algorithm as rl
import rl_utility as rlut
from env_gridworld import *

# env = Gridworld(10, Point2d(0, 0), Point2d(8, 8))
start_pos = Point2d(4, 0)
goal_pos = Point2d(8, 8)
dim = 10
env = Gridworld(dim, start_pos, goal_pos)
env.portal = (Point2d(2, 0), Point2d(4, 4))
env.wind_enable = False
env.wind_row = 5
env.wind_dir = WIND_DIR_U
env.wind_prob = 0.8

algorithm = sys.argv[1]
# filename = sys.argv[2]

np.random.seed(0)
q_table = None
stats = None

if algorithm == 'mc':
	settings = rl.SettingsMc()
	settings.sample_count = 500
	q_table, stats = rl.train_mc_first_visit(env, settings)
elif algorithm == 'ql':
	settings = rl.SettingsQLearning()
	settings.max_ep_count = 500
	settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, stats = rl.train_q_learning(env, settings)
elif algorithm == 'sarsa':
	settings = rl.SettingsSarsa()
	settings.max_ep_count = 500
	settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, stats = rl.train_sarsa(env, settings)
elif algorithm == 'ql-et':
	settings = rl.SettingsQlearningEt()
	settings.max_ep_count = 500
	settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, stats = rl.train_q_learning_et(env, settings)
elif algorithm == 'dq':
	settings = rl.SettingsDynaQ()
	settings.max_ep_count = 500
	settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, stats = rl.train_dyna_q(env, settings)
elif algorithm == 'ps':
	settings = rl.SettingsPrioritizedSweeping()
	settings.max_ep_count = 500
	settings.policy_fn = rl.PolicyEGreedyFn(epsilon=0.1)
	q_table, stats = rl.train_prioritized_sweeping(env, settings)
else:
	print('ERROR')
	sys.exit()

'''
print('Saving Q-table to ' + filename + ': ', end='')
np.save(filename, q_table)
print('DONE')
'''

print('Generating plots: ', end='')
rlut.statistics_plot(stats, algorithm)
rlut.statistics_plot_heatmap(stats, env, q_table, algorithm)
print('DONE')
